// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import App from './App'
import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
require('../node_modules/bootstrap/dist/css/bootstrap.min.css')
Vue.use(VueLocalStorage)

/* eslint-disable no-new */
new Vue({
  el: '#app1',
  template: '<App/>',
  components: { App }
})
